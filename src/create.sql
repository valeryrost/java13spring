create table reader
(
    id        serial primary key,
    firstName varchar(30) not null,
    surname   varchar(30) not null,
    birthday  date,
    phone     varchar(12),
    email     varchar(30),
    list_book varchar(100)
);
create table book
(
    id             serial primary key,
    title          varchar(50) not null,
    author         varchar(50) not null,
    year_publisher varchar(4),
    quantity_page  int
);
insert into book(title, author, year_publisher, quantity_page)
values ('Книга1', 'Автор1', '2001', 100);
insert into book(title, author, year_publisher, quantity_page)
values ('Книга2', 'Автор2', '2002', 200);
insert into book(title, author, year_publisher, quantity_page)
values ('Книга3', 'Автор3', '2003', 100);

drop table reader;
insert into reader (firstName, surName, birthDay, phone, email, list_book)
values ('Иван', 'Иванов', '2000-01-01', '1111111111', 'post1@mail.ru', '{"Книга1","Книга2"}');
insert into reader (firstName, surName, birthDay, phone, email, list_book)
values ('Петр', 'Петров', '2001-02-02', '2222222222', 'post2@mail.ru', 'Книга3');
select *
from reader;
select list_book
from reader
where phone = '1111111111';
select *
from reader
where phone = '2222222222';
select list_book
from reader
where phone = '3333333333';
select *
from book
where title = 'Книга2';

