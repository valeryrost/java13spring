package com.sber.java13spring.Java13SpringProject.dbexample.model;
/*
1. Создать таблицу клиента с полями:
● Фамилия
● Имя
● Дата рождения
● Телефон
● Почта
● Список названий книг из библиотеки
2. Создать класс UserDAO и Бин (prototype) этого класса.
3. Далее нам необходимо заполнить пользователей (т.е. у нас должен быть метод в коде,
который позволяет добавить пользователей в базу)
4. Написать метод, который принимает телефон/почту (на ваше усмотрение), достанет из
UserDAO список названий книг данного человека. С этим списком сходить в BookDAO и
получить всю информацию об этих книгах
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.relational.core.mapping.Column;

import java.time.LocalDate;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString



public class User {
    //  @Setter(AccessLevel.NONE)
    //  @Getter(AccessLevel.NONE)
    @Column
    private Integer userID;
    @Column
    private String firstName;
    @Column
    private String surName;
    @lombok.Getter
    @Column
    private String birthDay;
    @Column
    private String email;
    @Column
    private String phone;
    @Column
    private String  listBook;

    public User(String firstName, String surName, String birthDay, String email, String phone, String listBook) {
        this.firstName = firstName;
        this.surName = surName;
        this.birthDay = birthDay;
        this.email = email;
        this.phone = phone;
        this.listBook = listBook;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getListBook() {
        return listBook;
    }

    public void setListBook(String listBook) {
        this.listBook = listBook;
    }
}

