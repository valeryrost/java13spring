package com.sber.java13spring.Java13SpringProject.dbexample.constants;

public interface DBConsts {
    String DB_HOST = "localhost";
    String DB = "local_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";



    String DB_NAME = "reader";
    String USER_FIRSTNAME = "firstName";
    String USER_SURNAME =  "surName";
    String BIRTHDAY ="birthDay" ;
    String PHONE = "phone";
    String EMAIL = "email";
    String LIST_BOOK = "list_book";
    String DB_NAME_BOOK ="book";

}
