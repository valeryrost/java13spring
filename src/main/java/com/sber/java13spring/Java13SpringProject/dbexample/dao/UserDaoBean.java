package com.sber.java13spring.Java13SpringProject.dbexample.dao;
import com.sber.java13spring.Java13SpringProject.dbexample.constants.DBConsts;
import com.sber.java13spring.Java13SpringProject.dbexample.model.User;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;


@Component
public class UserDaoBean {

    private final Connection connection;


    public UserDaoBean(Connection connection) {
        this.connection = connection;
    }

    /*
    4. Написать метод, который принимает телефон/почту (на ваше усмотрение), достанет из
UserDAO список названий книг данного человека. С этим списком сходить в BookDAO и
получить всю информацию об этих книгах
     */


    /*
    3. Далее нам необходимо заполнить пользователей (т.е. у нас должен быть метод в коде,
    который позволяет добавить пользователей в базу)
     */
    public void singUpUser(String firstName, String surName, String birthDay, String email, String phone, String listBook) throws SQLException {
        String insert = "insert into " + DBConsts.DB_NAME + "(" + DBConsts.USER_FIRSTNAME + "," + DBConsts.USER_SURNAME + ","
                + DBConsts.BIRTHDAY + "," + DBConsts.PHONE + "," + DBConsts.EMAIL + "," + DBConsts.LIST_BOOK + ")" +
                "values(?,?,?,?,?,?)";
        PreparedStatement selectQuery = connection.prepareStatement(insert);

        selectQuery.setString(1, firstName);
        selectQuery.setString(2, surName);
        selectQuery.setString(3, birthDay);
        selectQuery.setString(4, phone);
        selectQuery.setString(5, email);
        selectQuery.setString(6, listBook);


        selectQuery.executeUpdate();


    }



    public User findUserByEmail(String email) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select list_book from reader where email = ?" );
        selectQuery.setString(1, email);
        ResultSet resultSet = selectQuery.executeQuery();
        User user = new User();
        while (resultSet.next()) {
//            user.setUserID(resultSet.getInt("id" ));
//            user.setFirstName(resultSet.getString("firstName" ));
//            user.setSurName(resultSet.getString("surName" ));
//            user.setBirthDay(resultSet.getString("birthDay" ));
//            user.setPhone(resultSet.getString("phone" ));
//            user.setEmail(resultSet.getString("email" ));
            user.setListBook(resultSet.getString("list_book" ));
            // System.out.println(resultSet.getString("list_book" ));
            // System.out.println(user);

            System.out.println(Arrays.asList(resultSet.getString("list_book").split(",")));
            //   System.out.println(resultSet);

        }
        // System.out.println(user);
        return user;
    }



}


