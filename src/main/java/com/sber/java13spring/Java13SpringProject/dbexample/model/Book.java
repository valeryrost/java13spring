package com.sber.java13spring.Java13SpringProject.dbexample.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.relational.core.mapping.Column;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Book {
    @Column
    private Integer id;
    @Column
    private String title;
    @Column
    private String author;
    @Column
    private String year_publisher;
    @Column
    private String quantity_page;

    public Integer getBookId() {
        return id;
    }

    public void setBookId(Integer bookId) {
        this.id = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getYear_publisher() {
        return year_publisher;
    }

    public void setYear_publisher(String year_publisher) {
        this.year_publisher = year_publisher;
    }

    public String getQuantity_page() {
        return quantity_page;
    }

    public void setQuantity_page(String quantity_page) {
        this.quantity_page = quantity_page;
    }
}



