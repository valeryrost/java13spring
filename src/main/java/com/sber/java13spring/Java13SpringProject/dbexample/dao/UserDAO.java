package com.sber.java13spring.Java13SpringProject.dbexample.dao;
import com.sber.java13spring.Java13SpringProject.dbexample.DB.DBApp;
import com.sber.java13spring.Java13SpringProject.dbexample.model.User;

import java.sql.*;

public class UserDAO {
    public User findUserByPhone(String phone) {
        try (Connection connection = DBApp.INSTANCE.newConnection()) {
            if (connection != null) {
                System.out.println("ура! мы подключились к БД!!!!");
            } else {
                System.out.println("база данных отдыхает, не трогайте!");
            }
            PreparedStatement selectQuery = connection.prepareStatement("select * from reader where phone = ?");
            selectQuery.setString(1, phone);
            ResultSet result = selectQuery.executeQuery();
            while (result.next()) {
                User user = new User();
                user.setUserID(result.getInt("id"));
                user.setFirstName(result.getString("firstName"));
                user.setSurName(result.getString("surName"));
                user.setBirthDay(result.getString("birthDay"));
                user.setEmail(result.getString("email"));
                user.setPhone(result.getString("phone"));
                user.setListBook(result.getString("list_book"));
                //  System.out.println(user);
                return user;
            }
        } catch (SQLException e) {
            System.out.println("Error:" + e.getMessage());
        }
        return null;
    }


}


