package com.sber.java13spring.Java13SpringProject.dbexample.dao;
import com.sber.java13spring.Java13SpringProject.dbexample.model.Book;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class BookDao {
    private  final Connection connection;
/*
...С этим списком сходить в BookDAO и
получить всю информацию об этих книгах
 */

    public BookDao(Connection connection) {
        this.connection = connection;
    }
    public  Book findUsersListBook(String books) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from book where title = ?");
        selectQuery.setString(1, books);
        ResultSet resultSet = selectQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setBookId(resultSet.getInt("id"));
            book.setTitle(resultSet.getString("title"));
            book.setAuthor(resultSet.getString("author"));
            book.setYear_publisher(resultSet.getString("year_publisher"));
            book.setQuantity_page(resultSet.getString("quantity_page"));

        }
        return book;
    }
}

