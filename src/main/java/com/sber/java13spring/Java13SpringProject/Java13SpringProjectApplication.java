package com.sber.java13spring.Java13SpringProject;

import com.sber.java13spring.Java13SpringProject.dbexample.dao.BookDao;
import com.sber.java13spring.Java13SpringProject.dbexample.dao.UserDaoBean;
import com.sber.java13spring.Java13SpringProject.dbexample.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

@SpringBootApplication
public class Java13SpringProjectApplication
        implements CommandLineRunner {
    private UserDaoBean userDaoBean;
    private BookDao bookDao;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public Java13SpringProjectApplication(UserDaoBean userDaoBean, BookDao bookDao, NamedParameterJdbcTemplate jdbcTemplate) {
        this.userDaoBean = userDaoBean;
        this.jdbcTemplate = jdbcTemplate;
        this.bookDao = bookDao;
    }

    public static void main(String[] args) {
        SpringApplication.run(Java13SpringProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(bookDao.findUsersListBook(String.valueOf(userDaoBean.findUserByEmail("post2@mail.ru").getListBook())));
        List<User> user = jdbcTemplate.query("select * from reader",
                (rs, rowNum) -> new User(
                        rs.getInt("id"),
                        rs.getString("firstName"),
                        rs.getString("surName"),
                        rs.getString("birthDay"),
                        rs.getString("email"),
                        rs.getString("phone"),
                        rs.getString("list_book")
                ));
        user.forEach(System.out::println);
    }
}

